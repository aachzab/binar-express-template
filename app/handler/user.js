const bcrypt = require('bcrypt')
const { user } = require('../model/user')

const showUser = async (req, res) => {
  try {
    const dbUser = await user.findAll({
      order: [
        ['id', 'DESC'],
        ['name', 'ASC'],
    ]
    })
    if(dbUser.length < 1) {
      res.status(404).json({
        message: "user data not found",
        data: dbUser
      }).end()
      return
    }
    
    res.status(200).json({
      message: "user data successfully fetched",
      data: dbUser
    }).end()
    return
  }catch(err){
    console.log(err)
    res.status(503).json({
      message: "internal server error",
      data: null
    })
    return
  }
}

const showNamaUser = async function(req, res){
  console.log(req.params.nama)
  const nama = req.params.nama
  
  const dbUser = await user.findOne({
    where: {
      name: nama
    }
  })
  if(!dbUser){
    res.status(404).json({
      message: `tidak ada yg namanya '${nama}'`,
      data: null
    }).end()
    return
  }
  res.status(200).json({
    message: `ada yg namanya '${nama}'`,
    data: dbUser
  }).end()
  return
}

const create = async function(req, res){
  let name = req.body.name
  let phone = req.body.phone
  let email = req.body.email
  let password = req.body.password

  const hashPassword = bcrypt.hashSync(password, 10)

  console.log(req.body)
  const data = await user.create({
    name: name,
    phone: phone,
    email: email,
    password: hashPassword
  })

  if(!data){
    res.status(400).json({
      message: "data gagal diinsert",
      data: null
    }).end()
    return
  }

  res.status(200).json({
    message: "data berhasil diinsert",
    data: data,
  }).end()
  return
}

const update = async function(req, res){
  let name = req.body.name
  let phone = req.body.phone
  let email = req.body.email
  let password = req.body.password
  let id = req.params.id

  console.log(req.body)
  const data = await user.update({
    name: name,
    phone: phone,
    email: email,
    password: password
  }, {
    where: {
      id: id
    }
  })

  if(!data){
    res.status(400).json({
      message: "data gagal diupdate",
      data: null
    }).end()
    return
  }

  res.status(200).json({
    message: "data berhasil diupdate",
    data: data,
  }).end()
  return
}

const deleted = async function(req, res){
  let id = req.params.id

  const data = await user.destroy({
    where: {
      id: id
    }
  })

  if(!data){
    res.status(400).json({
      message: "data gagal dihapus",
      data: null
    }).end()
    return
  }

  res.status(200).json({
    message: "data berhasil dihapus",
    data: data,
  }).end()
  return
}

const login = async function(req, res){
  let email = req.body.email
  let password = req.body.password

  const userLogin = await user.findOne({
    where: {
      email: email
    }
  })

  if(!userLogin){
    res.status(400).json({
      message: "Email tidak terdaftar pada sistem",
      data: null
    }).end()
    return
  }

  const isPasswordTrue = bcrypt.compareSync(password, userLogin.password)

  if(isPasswordTrue === false){
    res.status(400).json({
      message: "Password salah",
      data: null
    }).end()
    return
  }

  res.status(200).render('dashboard', {
    message: "Login berhasil",
    data: userLogin
  })
  return
}

module.exports = {
  showUser,
  showNamaUser,
  create,
  update,
  deleted,
  login
}