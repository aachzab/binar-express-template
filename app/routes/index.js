const express = require('express');
const router = express.Router();
const handler = require('../handler')

router.get('/', (req,res)=>{
  res.json({
    success: true
  })
})

router.get('/user/:nama', handler.user.showNamaUser)

router.get('/user', handler.user.showUser)

router.post('/user/create', handler.user.create)
router.put('/user/update/:id', handler.user.update)
router.get('/user/delete/:id', handler.user.deleted)

router.post('/user/login', handler.user.login)

module.exports = router;